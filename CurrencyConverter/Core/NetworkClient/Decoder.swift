//
//  Decoder.swift
//  CurrencyConverter
//
//  Created by k.malakhov on 08/09/2018.
//  Copyright © 2018 k.malakhov. All rights reserved.
//

import Foundation

protocol IJSONDecoder {
	func decode<T>(_ type: T.Type, from data: Data) throws -> T where T: Decodable
}

extension JSONDecoder: IJSONDecoder {}
