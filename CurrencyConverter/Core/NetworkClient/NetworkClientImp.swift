//
//  NetworkClientImp.swift
//  CurrencyConverter
//
//  Created by k.malakhov on 08/09/2018.
//  Copyright © 2018 k.malakhov. All rights reserved.
//

import Foundation

enum APIError: Error {
	case message(String)
}

final class NetworkClientImp {
	private let urlSession: IURLSession
	private let activityIndicator: NetworkActivityIndicator
	private let decoder: IJSONDecoder
	private let requestBuilder: RequestBuilder

	init(urlSession: IURLSession, activityIndicator: NetworkActivityIndicator, decoder: IJSONDecoder, requestBuilder: RequestBuilder ) {
		self.urlSession = urlSession
		self.activityIndicator = activityIndicator
		self.decoder = decoder
		self.requestBuilder = requestBuilder
	}
}

extension NetworkClientImp: NetworkClient {
	func load<T>(path: String?, method: HTTPMethod, params: [String: String], result: @escaping (Result<T>) -> Void) -> NetworkTask where T: Decodable {
		let request = requestBuilder.request(method: method, path: path, params: params)
		activityIndicator.showIndicator(true)
		let task = urlSession.dataTask(with: request) { [weak self] (data, _, error) in
			guard let `self` = self else { return }
			self.activityIndicator.showIndicator(false)

			if (error as NSError?)?.code == NSURLErrorCancelled {
				return
			}
			if let error = error {
				result(.fail(error))
				return
			}
			guard let data = data else {
				let error = APIError.message("Empty answer")
				result(.fail(error))
				return
			}

			do {
				let answer = try self.decoder.decode(T.self, from: data)
				result(.success(answer))
			} catch {
				result(.fail(error))
			}
		}
		task.resume()
		return task
	}
}
