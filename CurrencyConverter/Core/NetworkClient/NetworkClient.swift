//
//  NetworkClient.swift
//  CurrencyConverter
//
//  Created by k.malakhov on 08/09/2018.
//  Copyright © 2018 k.malakhov. All rights reserved.
//

import Foundation

// easy implimenation
protocol NetworkClient {
	@discardableResult
	func load<T>(path: String?, method: HTTPMethod, params: [String: String], result: @escaping (Result<T>) -> Void) -> NetworkTask where T: Decodable
}

enum HTTPMethod: String {
	case get = "GET"
	case post = "POST"
}

enum Result<T> {
	case fail(Error)
	case success(T)
}

public enum NetworkTaskState {
	case suspended
	case running
	case completed
	case cancelled
}

protocol NetworkTask: AnyObject {
	var taskState: NetworkTaskState { get }
	func cancel()
	func suspend()
	func resume()
}
