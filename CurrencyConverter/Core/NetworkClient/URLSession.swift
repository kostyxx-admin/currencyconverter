//
//  URLSession.swift
//  CurrencyConverter
//
//  Created by k.malakhov on 08/09/2018.
//  Copyright © 2018 k.malakhov. All rights reserved.
//

import Foundation

protocol IURLSession {
	func dataTask(with request: URLRequest, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Swift.Void) -> URLSessionDataTask
}

extension URLSession: IURLSession {}

extension URLSessionTask: NetworkTask {
	var taskState: NetworkTaskState {
		switch state {
		case .canceling:
			return .cancelled
		case .running:
			return .running
		case .suspended:
			return .suspended
		case .completed:
			return .completed
		}
	}
}
