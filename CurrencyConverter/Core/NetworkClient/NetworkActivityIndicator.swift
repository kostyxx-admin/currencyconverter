//
//  NetworkActivityIndicator.swift
//  CurrencyConverter
//
//  Created by k.malakhov on 08/09/2018.
//  Copyright © 2018 k.malakhov. All rights reserved.
//

import UIKit

protocol NetworkActivityIndicator {
	func showIndicator(_ show: Bool)
}

final class NetworkActivityIndicatorImp: NetworkActivityIndicator {
	var count = 0
	func showIndicator(_ show: Bool) {
		DispatchQueue.main.async { [weak self] in
			guard let `self` = self else { return }
			if show {
				self.count += 1
			} else {
				self.count -= 1
				if self.count < 0 {
					self.count = 0
				}
			}
			UIApplication.shared.isNetworkActivityIndicatorVisible = self.count != 0
		}
	}
}
