//
//  File.swift
//  CurrencyConverter
//
//  Created by k.malakhov on 08/09/2018.
//  Copyright © 2018 k.malakhov. All rights reserved.
//

import Foundation

protocol RequestBuilder {
	func request(method: HTTPMethod, path: String?, params: [String: String]?) -> URLRequest
}

final class RequestBuilderImp {
	private let baseApi: String
	init(baseAPI: String) {
		self.baseApi = baseAPI
	}
}

extension RequestBuilderImp: RequestBuilder {
	func request(method: HTTPMethod, path: String?, params: [String: String]?) -> URLRequest {
		guard var url = URL(string: baseApi) else {
			fatalError("Check base url: \(baseApi)")
		}

		if let path = path {
			url.appendPathComponent(path)
		}

		switch method {
		case .get:
			if let params = params {
				var urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: true)!
				urlComponents.queryItems = queryItems(params: params)
				if let urlWithComponents = urlComponents.url {
					url = urlWithComponents
				}
			}
		case .post:
			break
		}

		var request = URLRequest(url: url)
		request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
		request.httpMethod = method.rawValue
		return request
	}
}

private extension RequestBuilderImp {
	func queryItems(params: [String: String]) -> [URLQueryItem] {
		return params.keys.map { key -> URLQueryItem in
			URLQueryItem(name: key, value: params[key])
		}
	}
}
