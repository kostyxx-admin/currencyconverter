//
//  CurrencyService.swift
//  CurrencyConverter
//
//  Created by k.malakhov on 08/09/2018.
//  Copyright © 2018 k.malakhov. All rights reserved.
//

import Foundation

protocol CurrencyService {
	func load(base: String, complete: @escaping(Result<[String: Double]>) -> Void)
}

final class CurrencyServiceImp {
	struct CurrencyRates: Decodable {
		let rates: [String: Double]
	}

	private let networkClient: NetworkClient
    private weak var task: NetworkTask?

	init(networkClient: NetworkClient) {
		self.networkClient = networkClient
	}
}

extension CurrencyServiceImp: CurrencyService {
	func load(base: String, complete: @escaping(Result<[String: Double]>) -> Void) {
        task?.cancel()
		let params = ["base": base]
		task = networkClient.load(path: "latest", method: .get, params: params) { (result: Result<CurrencyRates>) in
			DispatchQueue.main.async {
				switch result {
				case .fail(let error):
					complete(.fail(error))
				case .success(let rates):
					complete(.success(rates.rates))
				}
			}
		}
	}
}
