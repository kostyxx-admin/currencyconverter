//
//  AppCoordinator.swift
//  CurrencyConverter
//
//  Created by k.malakhov on 16/09/2018.
//  Copyright © 2018 k.malakhov. All rights reserved.
//

import Foundation

import UIKit

final class AppCoordinator {
	private let window: UIWindow

	init(window: UIWindow) {
		self.window = window
	}

	func start() {
		let newsCoordinator = CurrenciesCoordinator(window: window)
		newsCoordinator.start()
	}
}
