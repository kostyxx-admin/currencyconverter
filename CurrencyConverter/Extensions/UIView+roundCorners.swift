//
//  UIView+Rounded.swift
//  CurrencyConverter
//
//  Created by k.malakhov on 16/09/2018.
//  Copyright © 2018 k.malakhov. All rights reserved.
//

import UIKit

extension UIView {
	@discardableResult
	func makeRounded() -> CAShapeLayer {
		return roundCorners(corners: .allCorners, radius: floor(bounds.height / 2))
	}

	@discardableResult
	func roundCorners(corners: UIRectCorner, radius: CGFloat) -> CAShapeLayer {
		let shape = CAShapeLayer()

		let cornerRadiiSize = CGSize(width: radius, height: radius)
		let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: cornerRadiiSize)

		shape.path = path.cgPath
		layer.mask = shape
		layer.masksToBounds = true

		return shape
	}
}
