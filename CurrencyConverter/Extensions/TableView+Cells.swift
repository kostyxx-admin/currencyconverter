//
//  TableView+Cells.swift
//  CurrencyConverter
//
//  Created by k.malakhov on 16/09/2018.
//  Copyright © 2018 k.malakhov. All rights reserved.
//

import UIKit

extension UITableView {
	func register<T: UITableViewCell>(_: T.Type) {
		let cellId = String(describing: T.self)
		let nib = UINib(nibName: cellId, bundle: nil)
		register(nib, forCellReuseIdentifier: cellId)
	}

	func dequeueCell<T: UITableViewCell>(_: T.Type, indexPath: IndexPath) -> UITableViewCell {
		let cellId = String(describing: T.self)
		let cell = dequeueReusableCell(withIdentifier: cellId, for: indexPath)
		return cell
	}
}
