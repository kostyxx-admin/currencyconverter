//
//  UIViewController+PresentingMessage.swift
//  CurrencyConverter
//
//  Created by k.malakhov on 16/09/2018.
//  Copyright © 2018 k.malakhov. All rights reserved.
//

import UIKit

struct AlertMessageModel {
	let title: String?
	let message: String
	struct Button {
		let title: String
		let action: () -> Void
	}
	let button: Button?
}

protocol AlertPresentable: AnyObject {
	func presentMessage(_ model: AlertMessageModel)
}

extension AlertPresentable where Self: UIViewController {
	func presentMessage(_ model: AlertMessageModel) {
		let alertController = UIAlertController(title: model.title, message: model.message, preferredStyle: .alert)
		if let buttonModel = model.button {
			let action = UIAlertAction(title: buttonModel.title, style: .default, handler: { _ in
				buttonModel.action()
			})
			alertController.addAction(action)
		}
		present(alertController, animated: true)
	}
}
