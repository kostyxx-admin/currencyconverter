//
//  ViewController.swift
//  CurrencyConverter
//
//  Created by k.malakhov on 16/09/2018.
//  Copyright © 2018 k.malakhov. All rights reserved.
//

import UIKit

protocol CurrenciesView: AnyObject {
	func changeOrder(fromTop toIndex: Int, toTop fromIndex: Int)
	func updateVisibleView()
	func reloadView()
}

final class CurrenciesVC: UIViewController {
	@IBOutlet private weak var tableView: UITableView!
	private let presenter: CurrenciesPresenter

	init(presenter: CurrenciesPresenter) {
		self.presenter = presenter
		super.init(nibName: nil, bundle: nil)
	}

	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	override func viewDidLoad() {
		super.viewDidLoad()

		tableView.keyboardDismissMode = .onDrag
		tableView.register(CurrencyCell.self)
		presenter.didLoad()
	}
}

extension CurrenciesVC: UITableViewDelegate {
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		presenter.didTap(at: indexPath.row)
	}
}

extension CurrenciesVC: UITableViewDataSource {
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return presenter.count
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		if let cell = tableView.dequeueCell(CurrencyCell.self, indexPath: indexPath) as? CurrencyCell {
			return cell
		}
		fatalError("Check cell type \(CurrencyCell.self)")
	}

	func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
		guard let cell = cell as? CurrencyCell
			else {
				return
		}
		let cellModel = presenter.model(for: indexPath.row)
		cell.configure(model: cellModel)
	}
}

extension CurrenciesVC: CurrenciesView {
	func updateVisibleView() {
		tableView.indexPathsForVisibleRows?.forEach({ (indexPath) in
			guard indexPath.row != 0 else { return }
			let cell = tableView.cellForRow(at: indexPath) as? CurrencyCell
			let cellModel = presenter.model(for: indexPath.row)
			cell?.configure(model: cellModel)
		})
	}

	func changeOrder(fromTop toIndex: Int, toTop fromIndex: Int) {
		let topIndex = IndexPath(row: 0, section: 0)
		let toIndex = IndexPath(item: toIndex, section: 0)
		let fromIndex = IndexPath(row: fromIndex, section: 0)

		tableView.contentOffset = .zero
		tableView.performBatchUpdates({
			tableView.moveRow(at: fromIndex, to: topIndex)
			tableView.moveRow(at: topIndex, to: toIndex)},
		completion: { [weak self] (_) in
			guard let `self` = self else { return }
			let topCell = self.tableView.cellForRow(at: topIndex) as? CurrencyCell
			let topModel = self.presenter.model(for: topIndex.row)
			topCell?.configure(model: topModel)
			topCell?.showKeyboard()

			let toCell = self.tableView.cellForRow(at: fromIndex) as? CurrencyCell
			let toModel = self.presenter.model(for: fromIndex.row)
			toCell?.configure(model: toModel)
		})
	}

	func reloadView() {
		tableView.reloadData()
	}
}

extension CurrenciesVC: AlertPresentable {}
