//
//  CurrenciesPresenter.swift
//  CurrencyConverter
//
//  Created by k.malakhov on 08/09/2018.
//  Copyright © 2018 k.malakhov. All rights reserved.
//

import UIKit

private enum Constants {
	static let defaultBase = "RUB"
	static let defaultEnteredValue: Double = 100
	static let updateTime: TimeInterval = 1
}

final class CurrenciesPresenter {
	private let numberFormatter: NumberFormatter
	private let currenciesService: CurrencyService
	private let countryByCurrency: [String: String]
	private let updateInterval: TimeInterval
	private var baseCurrency: String
	private var enteredValue: Double

	private var rates = [String: Double]()
	private var source = [String]()

	weak var view: (CurrenciesView & AlertPresentable)?

	init(currenciesService: CurrencyService, numberFormatter: NumberFormatter, countryByCurrency: [String: String] = countryByCurrencyCode,
		 baseCurrency: String = Constants.defaultBase, initEnteredValue: Double = Constants.defaultEnteredValue, updateInterval: TimeInterval
		= Constants.updateTime) {
		self.currenciesService = currenciesService
		self.numberFormatter = numberFormatter
		self.countryByCurrency = countryByCurrency
		self.baseCurrency = baseCurrency
		self.enteredValue = initEnteredValue
		self.updateInterval = updateInterval
	}

	func model(for index: Int) -> CurrencyCellModel {
		if index == 0 {
			let enterAction: (String) -> Void = {[weak self] value in
				self?.didEnter(text: value)
			}
			let valueString = numberFormatter.string(from: enteredValue as NSNumber)
			return CurrencyCellModel(flag: UIImage(named: baseCurrency.lowercased()),
									 state: .edit(action: enterAction),
									 name: baseCurrency,
									 countyName: countryByCurrency[baseCurrency],
									 value: valueString)
		} else {
			let currency = source[index]
			let value = (rates[currency] ?? 0) * enteredValue
			let valueString = numberFormatter.string(from: value as NSNumber)
			return CurrencyCellModel(flag: UIImage(named: currency.lowercased()),
									 state: .show,
									 name: currency,
									 countyName: countryByCurrency[currency],
									 value: valueString)
		}
	}

	var count: Int {
		return source.count
	}

	func didLoad() {
		initLoad()
	}

	func didTap(at newBaseIndex: Int) {
		guard newBaseIndex > 0 else { return }

		baseCurrency = source.remove(at: newBaseIndex)
		let oldBase = source.remove(at: 0)
		source.insert(baseCurrency, at: 0)

		let newOldBaseIndex = findIndex(for: oldBase)
		source.insert(oldBase, at: newOldBaseIndex)

		recalculateSimultaneously()
		view?.changeOrder(fromTop: newOldBaseIndex, toTop: newBaseIndex)
	}
}

private extension CurrenciesPresenter {

	func didRecieveCurrencies(_ rates: [String: Double]) {
		self.rates = rates

		source = rates.keys.sorted()
		self.rates[baseCurrency] = 1.0
		source.insert(baseCurrency, at: 0)
		view?.reloadView()
		infinityUpdate()
	}

    func updateCurrenciesRates(_ newRates: [String: Double]) {
        rates.keys.forEach { (key) in
            rates[key] = newRates[key]
        }
        rates[baseCurrency] = 1.0
		view?.updateVisibleView()
    }

	func didEnter(text: String) {
		var newValue: Double = 0
		if !text.isEmpty {
			guard let value = numberFormatter.number(from: text) as? Double else {
				return
			}
			newValue = value
		}
		guard newValue >= 0 else {
			return
		}
		
		enteredValue = newValue
        view?.updateVisibleView()
	}

	func findIndex(for oldBase: String) -> Int {
		if let searchIndex = source.enumerated()
			.dropFirst()
			.first(where: { $0.1 > oldBase }) {
			return searchIndex.offset
		} else {
			return source.endIndex
		}
	}

	func recalculateSimultaneously() {
		let newCurrencyRate = rates[baseCurrency] ?? 0
		rates.keys.forEach { (key) in
			if let rate = rates[key] {
				rates[key] = rate / newCurrencyRate
			}
		}
		enteredValue = newCurrencyRate * enteredValue
	}

	func initLoad() {
		currenciesService.load(base: baseCurrency) { [weak self] (result) in
			switch result {
			case .fail(let error):
				let buttonModel = AlertMessageModel.Button(title: "Try again", action: { [weak self] in
					self?.initLoad()
				})
				let messageModel = AlertMessageModel(title: "Loading error",
													 message: error.localizedDescription,
													 button: buttonModel)
				self?.view?.presentMessage(messageModel)
			case .success(let result):
				self?.didRecieveCurrencies(result)
			}
		}
	}
	
	func infinityUpdate() {
		currenciesService.load(base: baseCurrency) { [weak self] (result) in
			guard let `self` = self else { return }
			switch result {
			case .fail:
				break
			case .success(let result):
				self.updateCurrenciesRates(result)
			}
			DispatchQueue.main.asyncAfter(deadline: .now() + self.updateInterval, execute: {
				self.infinityUpdate()
			})
		}
	}
}
