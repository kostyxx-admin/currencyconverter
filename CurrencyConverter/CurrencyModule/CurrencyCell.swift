//
//  CurrencyCell.swift
//  CurrencyConverter
//
//  Created by k.malakhov on 16/09/2018.
//  Copyright © 2018 k.malakhov. All rights reserved.
//

import UIKit

struct CurrencyCellModel {
	enum State {
		case show
		case edit(action: (String) -> Void)
	}
	let flag: UIImage?
	let state: State
	let name: String
	let countyName: String?
	let value: String?
}

final class CurrencyCell: UITableViewCell {
	private enum Constants {
		static let grayUnderlineColor = UIColor.gray
		static let blueUnderlineColor = UIColor(red: 0.0, green: 122.0/255.0, blue: 1.0, alpha: 1.0)
	}
	@IBOutlet private weak var logoImageView: UIImageView!
	@IBOutlet private weak var titleLabel: UILabel!
	@IBOutlet private weak var textField: UITextField!
	@IBOutlet private weak var textFieldBottomBorderView: UIView!
	@IBOutlet private weak var countyNameLabel: UILabel!

	private var enterAction: ((String) -> Void)?

	override func awakeFromNib() {
		super.awakeFromNib()
		logoImageView.makeRounded()
		textField.delegate = self
		textFieldBottomBorderView.backgroundColor = Constants.grayUnderlineColor
	}

	override func prepareForReuse() {
		super.prepareForReuse()
		enterAction = nil
	}

    func configure(model: CurrencyCellModel) {
		logoImageView.image = model.flag
		titleLabel.text = model.name
		countyNameLabel.text = model.countyName
		switch model.state {
		case .edit(let action):
			textField.isEnabled = true
			enterAction = action
		case .show:
            enterAction = nil
			textField.isEnabled = false
		}
		textField.text = model.value
	}

	func showKeyboard() {
		textField.becomeFirstResponder()
	}
}

extension CurrencyCell: UITextFieldDelegate {
	func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
		if let text = textField.text,
			let textRange = Range(range, in: text) {
			let enteredText = text.replacingCharacters(in: textRange, with: string)
			enterAction?(enteredText)
		}
		return true
	}

	func textFieldDidBeginEditing(_ textField: UITextField) {
		textFieldBottomBorderView.backgroundColor = Constants.blueUnderlineColor
	}

	func textFieldDidEndEditing(_ textField: UITextField) {
		textFieldBottomBorderView.backgroundColor = Constants.grayUnderlineColor
	}
}
