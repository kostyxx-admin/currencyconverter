//
//  CurrenciesCoordinator.swift
//  CurrencyConverter
//
//  Created by k.malakhov on 16/09/2018.
//  Copyright © 2018 k.malakhov. All rights reserved.
//

import UIKit

final class CurrenciesCoordinator {
	private enum Constants {
		static let baseURL = "https://revolut.duckdns.org/"
	}
	private let window: UIWindow

	init(window: UIWindow) {
		self.window = window
	}

	func start() {
		let currenciesVC = createViewController()
		window.rootViewController = currenciesVC
		window.makeKeyAndVisible()
	}
	
	private func createViewController() -> CurrenciesVC {
		let urlSession = URLSession(configuration: URLSessionConfiguration.default)
		let networkIndicator = NetworkActivityIndicatorImp()
		let decoder = JSONDecoder()
		let requestBuilder = RequestBuilderImp(baseAPI: Constants.baseURL)
		let networkClient: NetworkClient = NetworkClientImp(urlSession: urlSession,
															activityIndicator: networkIndicator,
															decoder: decoder,
															requestBuilder: requestBuilder)
		let currencyService: CurrencyService = CurrencyServiceImp(networkClient: networkClient)
		
		let numberFormatter = NumberFormatter()
		numberFormatter.groupingSeparator = ""
		numberFormatter.usesGroupingSeparator = true
		numberFormatter.decimalSeparator = ","
		numberFormatter.numberStyle = .decimal
		numberFormatter.maximumFractionDigits = 2
		numberFormatter.minimumFractionDigits = 0
		
		let presenter = CurrenciesPresenter(currenciesService: currencyService,
											numberFormatter: numberFormatter,
											countryByCurrency: countryByCurrencyCode)
		let currenciesVC = CurrenciesVC(presenter: presenter)
		presenter.view = currenciesVC
		return currenciesVC
	}
}
