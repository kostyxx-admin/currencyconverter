//
//  Mocks.swift
//  CurrencyConverterTests
//
//  Created by k.malakhov on 16/09/2018.
//  Copyright © 2018 k.malakhov. All rights reserved.
//

import Foundation
@testable import CurrencyConverter

class CurrencyServiceMock: CurrencyService {
    var invokedLoad = false
    var invokedLoadCount = 0
    var invokedLoadParameters: (base: String, Void)?
    var invokedLoadParametersList = [(base: String, Void)]()
    var stubbedLoadCompleteResult: (Result<[String: Double]>, Void)?
    func load(base: String, complete: @escaping(Result<[String: Double]>) -> Void) {
        invokedLoad = true
        invokedLoadCount += 1
        invokedLoadParameters = (base, ())
        invokedLoadParametersList.append((base, ()))
        if let result = stubbedLoadCompleteResult {
            complete(result.0)
        }
    }
}

class CurrenciesViewMock: CurrenciesView, AlertPresentable {
    var invokedChangeOrder = false
    var invokedChangeOrderCount = 0
    var invokedChangeOrderParameters: (toIndex: Int, fromIndex: Int)?
    var invokedChangeOrderParametersList = [(toIndex: Int, fromIndex: Int)]()
    func changeOrder(fromTop toIndex: Int, toTop fromIndex: Int) {
        invokedChangeOrder = true
        invokedChangeOrderCount += 1
        invokedChangeOrderParameters = (toIndex, fromIndex)
        invokedChangeOrderParametersList.append((toIndex, fromIndex))
    }
    var invokedUpdateVisibleView = false
    var invokedUpdateVisibleViewCount = 0
    func updateVisibleView() {
        invokedUpdateVisibleView = true
        invokedUpdateVisibleViewCount += 1
    }
    var invokedReloadView = false
    var invokedReloadViewCount = 0
    func reloadView() {
        invokedReloadView = true
        invokedReloadViewCount += 1
    }
    var invokedPresentMessage = false
    var invokedPresentMessageCount = 0
    var invokedPresentMessageParameters: (model: AlertMessageModel, Void)?
    var invokedPresentMessageParametersList = [(model: AlertMessageModel, Void)]()
    func presentMessage(_ model: AlertMessageModel) {
        invokedPresentMessage = true
        invokedPresentMessageCount += 1
        invokedPresentMessageParameters = (model, ())
        invokedPresentMessageParametersList.append((model, ()))
    }
}

class NetworkClientMock<D: Decodable>: NetworkClient {
    var invokedLoad = false
    var invokedLoadCount = 0
    var invokedLoadParameters: (path: String?, method: HTTPMethod, params: [String: String])?
    var invokedLoadParametersList = [(path: String?, method: HTTPMethod, params: [String: String])]()
    var stubbedLoadResultResult: (Result<D>, Void)?
    var stubbedLoadResult: NetworkTask!
    @discardableResult
    func load<T>(path: String?, method: HTTPMethod, params: [String: String], result: @escaping (Result<T>) -> Void) -> NetworkTask where T: Decodable {
        invokedLoad = true
        invokedLoadCount += 1
        invokedLoadParameters = (path, method, params)
        invokedLoadParametersList.append((path, method, params))
        if let stubbed = stubbedLoadResultResult?.0 as? Result<T> {
            result(stubbed)
        }
        return stubbedLoadResult
    }
}

class NetworkTaskMock: NetworkTask {
    var invokedTaskStateGetter = false
    var invokedTaskStateGetterCount = 0
    var stubbedTaskState: NetworkTaskState!
    var taskState: NetworkTaskState {
        invokedTaskStateGetter = true
        invokedTaskStateGetterCount += 1
        return stubbedTaskState
    }
    var invokedCancel = false
    var invokedCancelCount = 0
    func cancel() {
        invokedCancel = true
        invokedCancelCount += 1
    }
    var invokedSuspend = false
    var invokedSuspendCount = 0
    func suspend() {
        invokedSuspend = true
        invokedSuspendCount += 1
    }
    var invokedResume = false
    var invokedResumeCount = 0
    func resume() {
        invokedResume = true
        invokedResumeCount += 1
    }
}
