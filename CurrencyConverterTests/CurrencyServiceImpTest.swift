//
//  CurrencyServiceImpTest.swift
//  CurrencyConverterTests
//
//  Created by k.malakhov on 17/09/2018.
//  Copyright © 2018 k.malakhov. All rights reserved.
//

import XCTest
@testable import CurrencyConverter

class CurrencyServiceImpTest: XCTestCase {
	
	var networkServiceMock: NetworkClientMock<CurrencyServiceImp.CurrencyRates>!
	var currencyService: CurrencyServiceImp!
	
    override func setUp() {
        super.setUp()
		networkServiceMock = NetworkClientMock()
		currencyService = CurrencyServiceImp(networkClient: networkServiceMock)
    }
    
    override func tearDown() {
        super.tearDown()
    }
	
	func testSuccess() {
		// given
		let dict = ["RUB": 100.0,
					"USD": 5.0]
		let currency = CurrencyServiceImp.CurrencyRates(rates: dict)
		networkServiceMock.stubbedLoadResultResult = (.success(currency), ())
		networkServiceMock.stubbedLoadResult = NetworkTaskMock()
		let expectation = XCTestExpectation()
		// when
		
		var result: Result<[String: Double]> = .fail(NSError(domain: "test", code: 0, userInfo: nil))
		currencyService.load(base: "RUB") { (checkResult) in
			result = checkResult
			expectation.fulfill()
		}
		
		wait(for: [expectation], timeout: 0.01)
		
		//then
		switch result {
		case .fail:
			XCTFail()
		case .success(let result):
			XCTAssertEqual(result, dict)
		}
	}
	
	func testFail() {
		// given
		let error = NSError(domain: "test", code: 0, userInfo: nil)
		networkServiceMock.stubbedLoadResultResult = (.fail(error), ())
		networkServiceMock.stubbedLoadResult = NetworkTaskMock()
		let expectation = XCTestExpectation()
		
		// when
		let dict = ["RUB": 100.0,
					"USD": 5.0]
		var expectedResult: Result<[String: Double]> = Result.success(dict)
		currencyService.load(base: "RUB") { (checkResult) in
			expectedResult = checkResult
			expectation.fulfill()
		}
		
		wait(for: [expectation], timeout: 0.01)
		
		//then
		switch expectedResult {
		case .fail(let serviceError):
			XCTAssertEqual(error, serviceError as NSError)
		case .success:
			XCTFail()
		}
	}
	
	func testNetworkOperationCancel() {
		// given
		let error = NSError(domain: "test", code: 0, userInfo: nil)
		networkServiceMock.stubbedLoadResultResult = (.fail(error), ())
		let firstTask = NetworkTaskMock()
		
		networkServiceMock.stubbedLoadResult = firstTask
		// when
		currencyService.load(base: "base") { _ in
			
		}
		let secondTask = NetworkTaskMock()
		networkServiceMock.stubbedLoadResult = secondTask
		currencyService.load(base: "base") { _ in
			
		}
		// then
		XCTAssertTrue(firstTask.invokedCancel)
	}
	
	func testParams() {
		// given
		let baseCurrency = "RUB"
		let params = ["base": baseCurrency]
		networkServiceMock.stubbedLoadResult = NetworkTaskMock()
		// when
		currencyService.load(base: baseCurrency) { _ in
			
		}
		// then
		XCTAssertEqual(networkServiceMock.invokedLoadParameters?.params, params)
	}
}
