//
//  CurrenciesPresenterTest.swift
//  CurrencyConverterTests
//
//  Created by k.malakhov on 16/09/2018.
//  Copyright © 2018 k.malakhov. All rights reserved.
//

import XCTest
@testable import CurrencyConverter

class CurrenciesPresenterTest: XCTestCase {
	enum TestCurrencies: String {
		case EUR
		case RUB
		case USD
	}
	
	lazy var numberFormatter: NumberFormatter = {
		let numberFormatter = NumberFormatter()
		numberFormatter.groupingSeparator = ""
		numberFormatter.usesGroupingSeparator = true
		numberFormatter.decimalSeparator = ","
		numberFormatter.numberStyle = .decimal
		numberFormatter.maximumFractionDigits = 2
		numberFormatter.minimumFractionDigits = 0
		return numberFormatter
	}()
	
	let currenciesRates: [String: Double] = [TestCurrencies.RUB.rawValue: 3.0,
											 TestCurrencies.EUR.rawValue: 2.0]
	let initBaseCurrency = TestCurrencies.USD.rawValue
	let initEnteredValue: Double = 150
	
	var presenter: CurrenciesPresenter!
	var currencyServiceMock: CurrencyServiceMock!
	var curreniesView: CurrenciesViewMock!
	
	let updateInterval: TimeInterval = 0.4
	
	override func setUp() {
		super.setUp()
		
		currencyServiceMock = CurrencyServiceMock()
		curreniesView = CurrenciesViewMock()
		presenter = CurrenciesPresenter(currenciesService: currencyServiceMock, numberFormatter: numberFormatter, baseCurrency: initBaseCurrency,
										initEnteredValue: initEnteredValue, updateInterval: updateInterval)
		presenter.view = curreniesView
	}
	
	func testDidLoadBaseCurreny() {
		// given
		// when
		presenter.didLoad()
		// then
		XCTAssertEqual(currencyServiceMock.invokedLoadParameters?.base, initBaseCurrency)
	}
	
	func testInitLoadErrorMessage() {
		// given
		let messageTest = "ErrorText"
		let error = NSError(domain: "test", code: 1, userInfo:  [NSLocalizedDescriptionKey: messageTest])
		currencyServiceMock.stubbedLoadCompleteResult = (Result<[String: Double]>.fail(error), ())
		// when
		presenter.didLoad()
		// then
		XCTAssertEqual(curreniesView.invokedPresentMessageParameters?.model.message, messageTest)
	}
	
	func testInitLoadErrorTryAgain() {
		// given
		let messageTest = "ErrorText"
		let error = NSError(domain: "test", code: 1, userInfo:  [NSLocalizedDescriptionKey: messageTest])
		currencyServiceMock.stubbedLoadCompleteResult = (.fail(error), ())
		// when
		presenter.didLoad()
		guard let button = curreniesView.invokedPresentMessageParameters?.model.button else {
			XCTFail()
			return
		}
		button.action()
		// then
		XCTAssertEqual(currencyServiceMock.invokedLoadCount, 2)
	}
	
	func testInitLoadSuccess() {
		// given
		currencyServiceMock.stubbedLoadCompleteResult = (.success(currenciesRates), ())
		// when
		presenter.didLoad()
		// then
		XCTAssertTrue(curreniesView.invokedReloadView)
	}
	
	func testBaseCurrencyModel() {
		// given
		currencyServiceMock.stubbedLoadCompleteResult = (.success(currenciesRates), ())
		// when
		presenter.didLoad()
		// then
		let baseModel = presenter.model(for: 0)
		XCTAssertEqual(baseModel.name, initBaseCurrency)
	}
	
	func testModelOrder() {
		// given
		currencyServiceMock.stubbedLoadCompleteResult = (.success(currenciesRates), ())
		// when
		presenter.didLoad()
		// then
		XCTAssertEqual(presenter.model(for: 1).name, TestCurrencies.EUR.rawValue)
		XCTAssertEqual(presenter.model(for: 2).name, TestCurrencies.RUB.rawValue)
	}
	
	func testInitModelValuesCalc() {
		// given
		currencyServiceMock.stubbedLoadCompleteResult = (.success(currenciesRates), ())
		
		let eurValue = numberFormatter.string(from: 300 as NSNumber)
		let rubValue = numberFormatter.string(from: 450 as NSNumber)
		
		// when
		presenter.didLoad()
		
		// then
		XCTAssertEqual(presenter.model(for: 1).value, eurValue)
		XCTAssertEqual(presenter.model(for: 2).value, rubValue)
	}
	
	
	func testDidEnterUpdateVisibleViewInvocation() {
		// given
		currencyServiceMock.stubbedLoadCompleteResult = (.success(currenciesRates), ())
		
		// when
		presenter.didLoad()
		let viewModel = presenter.model(for: 0)
		guard case .edit(let action) = viewModel.state else {
			XCTFail()
			return
		}
		action("100")
		
		// then
		XCTAssertTrue(curreniesView.invokedUpdateVisibleView)
	}
	
	func testDidEnterCorrectNewValues() {
		// given
		currencyServiceMock.stubbedLoadCompleteResult = (.success(currenciesRates), ())
		let newEnteredValue = "100"
		// when
		presenter.didLoad()
		let viewModel = presenter.model(for: 0)
		guard case .edit(let action) = viewModel.state else {
			XCTFail()
			return
		}
		action(newEnteredValue)
		
		// then
		let eurValue = numberFormatter.string(from: 200 as NSNumber)
		let rubValue = numberFormatter.string(from: 300 as NSNumber)
		XCTAssertEqual(presenter.model(for: 1).value, eurValue)
		XCTAssertEqual(presenter.model(for: 2).value, rubValue)
	}
	
	func testDidEnterInCorrectValue() {
		// given
		currencyServiceMock.stubbedLoadCompleteResult = (.success(currenciesRates), ())
		let newEnteredValue = "adsf4352"
		// when
		presenter.didLoad()
		let viewModel = presenter.model(for: 0)
		guard case .edit(let action) = viewModel.state else {
			XCTFail()
			return
		}
		action(newEnteredValue)
		
		// then
		XCTAssertNotEqual(curreniesView.invokedUpdateVisibleViewCount, 2)
	}
	
	func testDidEnterEmptyValue() {
		// given
		currencyServiceMock.stubbedLoadCompleteResult = (.success(currenciesRates), ())
		
		// when
		presenter.didLoad()
		let viewModel = presenter.model(for: 0)
		guard case .edit(let action) = viewModel.state else {
			XCTFail()
			return
		}
		action("")
		
		// then
		XCTAssertEqual(presenter.model(for: 1).value, "0")
		XCTAssertEqual(presenter.model(for: 2).value, "0")
	}
	
	func testDidEnterNegativeNumber() {
		// given
		currencyServiceMock.stubbedLoadCompleteResult = (.success(currenciesRates), ())
		
		// when
		presenter.didLoad()
		let viewModel = presenter.model(for: 0)
		guard case .edit(let action) = viewModel.state else {
			XCTFail()
			return
		}
		action("-150")
		
		// then
		XCTAssertNotEqual(curreniesView.invokedUpdateVisibleViewCount, 2)
	}
	
	func testDidChooseAnotherCurrency() {
		// given
		currencyServiceMock.stubbedLoadCompleteResult = (.success(currenciesRates), ())
		// when
		presenter.didLoad()
		presenter.didTap(at: 2)
		// then
		XCTAssertEqual(curreniesView.invokedChangeOrderParametersList[0].toIndex, 2)
		XCTAssertEqual(curreniesView.invokedChangeOrderParametersList[0].fromIndex, 2)
	}
	
	func testDidTapOnSameBaseCurrency() {
		// given
		currencyServiceMock.stubbedLoadCompleteResult = (.success(currenciesRates), ())
		// when
		presenter.didLoad()
		presenter.didTap(at: 0)
		// then
		XCTAssertFalse(curreniesView.invokedChangeOrder)
	}
	
	func testModelValuesDidChooseAnotherCurrency() {
		// given
		currencyServiceMock.stubbedLoadCompleteResult = (.success(currenciesRates), ())
		// when
		presenter.didLoad()
		presenter.didTap(at: 2)
		// then
		
		let baseModel = presenter.model(for: 0)
		XCTAssertEqual(baseModel.name, TestCurrencies.RUB.rawValue)
		XCTAssertEqual(baseModel.value, "450")
		let firstModel = presenter.model(for: 1)
		XCTAssertEqual(firstModel.name, TestCurrencies.EUR.rawValue)
		XCTAssertEqual(firstModel.value, "300")
		
		let lastModel = presenter.model(for: 2)
		let expectedLastValue = numberFormatter.string(from: initEnteredValue as NSNumber)
		XCTAssertEqual(lastModel.name, TestCurrencies.USD.rawValue)
		XCTAssertEqual(lastModel.value, expectedLastValue)
	}
	
	func testInfinityCurrenciesRateSuccessUpdates() {
		// given
		currencyServiceMock.stubbedLoadCompleteResult = (.success(currenciesRates), ())
		let expectation = XCTestExpectation()
		// when
		presenter.didLoad()
		
		DispatchQueue.main.asyncAfter(deadline: .now() + updateInterval * 3) {
			expectation.fulfill()
		}
		
		wait(for: [expectation], timeout: updateInterval * 3)
		XCTAssertEqual(currencyServiceMock.invokedLoadCount, 4)
	}
	
	func testInfinityCurrenciesRateSuccessUpdatesInvokeViewUpdate() {
		// given
		currencyServiceMock.stubbedLoadCompleteResult = (.success(currenciesRates), ())
		let expectation = XCTestExpectation()
		// when
		presenter.didLoad()
		
		DispatchQueue.main.asyncAfter(deadline: .now() + updateInterval) {
			expectation.fulfill()
		}
		
		wait(for: [expectation], timeout: updateInterval + 0.1)
		XCTAssertEqual(currencyServiceMock.invokedLoadCount, 3)
	}
	
	func testInfinityCurrenciesRateErrorUpdates() {
		// given
		currencyServiceMock.stubbedLoadCompleteResult = (.success(currenciesRates), ())
		let expectation = XCTestExpectation()
		// when
		presenter.didLoad()
		
		let someError = NSError(domain: "test", code: 0, userInfo: nil)
		currencyServiceMock.stubbedLoadCompleteResult = (.fail(someError), ())
		DispatchQueue.main.asyncAfter(deadline: .now() + updateInterval * 3) {
			expectation.fulfill()
		}
		
		wait(for: [expectation], timeout: updateInterval * 3)
		XCTAssertEqual(currencyServiceMock.invokedLoadCount, 4, "shoud try update again")
	}
	
	func testInfinityCurrenciesRateErrorUpdatesInvokeViewUpdate() {
		// given
		currencyServiceMock.stubbedLoadCompleteResult = (.success(currenciesRates), ())
		let expectation = XCTestExpectation()
		// when
		presenter.didLoad()
		let someError = NSError(domain: "test", code: 0, userInfo: nil)
		
		DispatchQueue.main.asyncAfter(deadline: .now() + updateInterval - 0.1) {
			self.currencyServiceMock.stubbedLoadCompleteResult = (.fail(someError), ())
			expectation.fulfill()
		}
		
		wait(for: [expectation], timeout: updateInterval + 0.1)
		XCTAssertNotEqual(curreniesView.invokedUpdateVisibleViewCount, 2, "shoudn update view with error")
	}
	
	func testLoadWithNewChosenCurrency() {
		// given
		currencyServiceMock.stubbedLoadCompleteResult = (.success(currenciesRates), ())
		let expectation = XCTestExpectation()
		let newBaseCurrense = TestCurrencies.RUB
		// when
		presenter.didLoad()
		presenter.didTap(at: 2)
		DispatchQueue.main.asyncAfter(deadline: .now() + updateInterval) {
			expectation.fulfill()
		}
		
		// then
		wait(for: [expectation], timeout: updateInterval + 0.1)
		XCTAssertEqual(currencyServiceMock.invokedLoadParameters?.base, newBaseCurrense.rawValue)
	}
	
	func testModelCount() {
		// given
		currencyServiceMock.stubbedLoadCompleteResult = (.success(currenciesRates), ())
		// when
		presenter.didLoad()
		// then
		XCTAssertEqual(presenter.count, currenciesRates.count + 1)
	}
}
